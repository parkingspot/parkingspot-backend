# parkingspot-backend

This is the central project for publishing ParkingSpot RESTful API microservices in a managed, monitored, secure, scalable environment.

**It links the frontend, API gateway and microservices together with Git submodules, be sure to clone the repository with `git clone --recursive`.**

## Overview

Component projects in sub-directories, created with JHipster:

- `api-gateway`: API gateway application
    - Spring Boot application that serves the Angular admin application and exposes internal REST APIs
    - exposes REST API documentation with Swagger
    - uses Netflix Zuul for dynamic routing, monitoring, resiliency, security
    - uses Netflix Ribbon for communication with microservices via JHipster Registry Eureka server

- `customer-api`: customer API microservice application
    - Spring Boot application for serving the REST API
    - registers in JHipster Registry automatically upon startup

- `parking-api`: parking API microservice application
    - Spring Boot application for serving the REST API
    - registers in JHipster Registry automatically upon startup

- `docker-compose`: Docker Compose configuration to run all applications and services together
- `kubernetes`: Kubernetes configuration to run all applications and services in a Kubernetes cluster

Required services the above projects depend on:

- **JHipster Registry** service registry and configuration server
    - uses Netflix Eureka for REST service registry/discovery, loadbalancer, failover
    - uses Spring Cloud Configuration for keeping configuration in Git
    - exposes dashboards in admin application to monitor and manage applications

- **JHipster Console** monitoring and alerting server
    - based on Elasticsearch, Logstash, Kibana (ELK) stack
    - Elastalert for alerting

- **Zipkin** (optional, not configured yet)
    - Distributed tracing

- **PostgreSQL** database server
    - Not strictly needed during development, each microservice has a dedicated DB in Docker Compose and Kubernetes configurations

## Run manually

### Prerequisites

1. Java SDK

### Running

1. Download and run the latest [JHipster Registry release](https://github.com/jhipster/jhipster-registry/releases) WAR

2. Run services and gateway in `customer-api`, `parking-api` and `api-gateway` with

        ./mvnw

3. Open the [administration application](http://localhost:8080) in browser,
   explore API docs and gateway, add entities to explore how it works

## Run locally with Docker Compose

### Prerequisites

1. Java SDK
2. Docker

### Build Docker images

1. Run Docker

2. In `customer-api`, `parking-api` and `api-gateway` build Docker images with

        ./mvnw -DskipTests=true -Pprod dockerfile:build

Use `-DskipTests=true` if you don't want to run tests. Also, you might want to do a clean build with `clean package`.

### Run with Docker Compose

1. Run Docker

2. To start all applications and services together, in `docker-compose`

        docker-compose up -d

3. Status

        docker-compose ps

4. Live logs

        docker-compose logs -f

5. Stop and remove containers (required for installing new versions of images)

        docker-compose down


### Memory limits

It is possible to limit the memory each container uses with the `mem_limit`
parameter in `docker-compose.yml` file or with the `-m` `docker` command-line
argument. However, due to the nature of how JVM allocates memory, this
parameter alone is not enough to safely constrict the memory of Docker
containers running Java applications, additionally it is necessary to set the
`-Xmx` (and `-Xms`) parameters to cap the heap memory size. It is recommended
to set the JVM heap size 2.5-3 times less than Docker container's `mem_limit`
(*TODO: this claim is copy-pasted, it needs rationale and verification*).

Depending on how the Docker image is configured in original Dockerfile, it
should be possible to pass the JVM parameters in an environment variable to the
container. JHipster-related containers accept the environment variable
`_JAVA_OPTIONS` or, in the case of Elasticsearch, `ES_JAVA_OPTS`.

##### Memory settings for applications

Following is an overview of usable memory settigs to run the entire stack on a
local machine using about 2G ram.

|service                 | `mem_limit` | Docker env. variable              |
|------------------------|-------------|-----------------------------------|
|apigateway              |   512m      | `_JAVA_OPTIONS=-Xmx200m -Xms200m` | 
|customerapi             |   512m      | `_JAVA_OPTIONS=-Xmx200m -Xms200m` |
|parkingapi              |   512m      | `_JAVA_OPTIONS=-Xmx200m -Xms200m` |
|jhipster-console        |   384m      | `_JAVA_OPTIONS=-Xmx100m -Xms100m` |
|jhipster-elasticsearch  |   512m      | `ES_JAVA_OPTS=-Xmx200m -Xms200m`  |
|jhipster-logstash       |   512m      | `_JAVA_OPTIONS=-Xmx200m -Xms200m` |
|jhipster-registry       |   512m      | `_JAVA_OPTIONS=-Xmx200m -Xms200m` |
|jhipster-zipkin         |   384m      | `_JAVA_OPTIONS=-Xmx100m -Xms100m` |
|postgres                |   64m       |                                   |

##### Troubleshooting memory settings

If the given JVM options do not seem to limit memory usage, then connect to the
running container and inspect the run command, process and environment
variables

1. Connect to the container using `docker exec -it <container id> sh`

2. Inspect running processes using `top`, `ps` and also the container
   entrypoint script. NB! `free` and `top` commands inside the container do not
   report correct values (e.g. values set by `mem_limit`), see [this article](https://developers.redhat.com/blog/2017/03/14/java-inside-docker/)
   for explanation.

## Run with Kuberenetes

### Prerequisites

1. [Google Cloud SDK](https://cloud.google.com/sdk/docs/quickstart-windows)
2. [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/#download-as-part-of-the-google-cloud-sdk)
3. Account configured by admins in [Google Cloud Platform](https://console.cloud.google.com/)
5. [Minikube](https://kubernetes.io/docs/getting-started-guides/minikube/#installation)
2. Hyper-V or VirtualBox for Minikube

### Run in a local cluster with Minikube

1. Start local cluster with `minikube` (default 2 CPU cores, 2 GB memory and 20 GB disk)

        minikube start # --vm-driver=hyperv in Windows when using Hyper-V

    - Wait until `Kubectl is now configured to use the cluster.`

2. Configure your Docker client to use the Docker daemon that runs inside Minikube and make the Docker images available there

        eval $(minikube docker-env)
        sudo ifconfig vboxnet0 up # in Linux, if kubectl says 'Unable to connect'

        ./build.sh

        for app in apigateway customerapi parkingapi
        do
            docker tag $app eu.gcr.io/parkingspot/$app:v0.1
        done

    - Alternatively, [configure access from Minikube to private container registry](https://github.com/kubernetes/minikube/blob/master/docs/insecure_registry.md#private-container-registries)

            minikube addons configure registry-creds
            minikube addons enable registry-creds

1. Open Minikube Kubernetes Dashboard

        minikube dashboard

2. Deploy all your apps by running:

        for app in registry apigateway customerapi parkingapi
        do
            kubectl apply -f $app
        done

3. Examine logs

        kubectl logs customerapi-1825525273-ft7j4

3. Find your application's IP address:

        kubectl get svc apigateway

4. Remove deployed resources when done

        for app in registry apigateway customerapi parkingapi
        do
            kubectl delete -f $app
        done

2. Finally, stop the cluster

        minikube stop

1. Optionally, delete the cluster

        minikube delete

Useful Minikube commands:

- `minikube ssh`: login to Minikube VM with SSH

### Prepare project in Google Cloud Platform

1. Create new cloud project in Google Cloud Console
    - This is already done by admins
    - [login **with right account**](https://console.cloud.google.com/)
    - Choose *Create project*, name it *parkingspot*
    - The project name will be visible in top left header, next to Google Cloud Platform title

2. Activate Google Container Registry service for the project
    - Open [Container Registry section](https://console.cloud.google.com/gcr/images/parkingspot?project=parkingspot) in Google Cloud Console to activate it
    - **Be sure to activate Container Registry before Container Engine**, otherwise Container Engine might not be able to access containers from the registry

3. Activate Google Container Engine service for the project
    - Open [Container Engine section](https://console.cloud.google.com/kubernetes/list?project=parkingspot) in Google Cloud Console to activate it

### Publish Docker images to private Google Container Registry

1. Prerequisites:
    - Container Registry activated
    - Docker images built

2. Login to Google Cloud Console

        gcloud auth application-default login

3. Tag and push applications

        DOCKER_REPOSITORY_BASE=eu.gcr.io/parkingspot
        for app in apigateway customerapi parkingapi
        do
          docker tag $app $DOCKER_REPOSITORY_BASE/$app
          gcloud docker -- push $DOCKER_REPOSITORY_BASE/$app
        done

    - If you get `Permission denied: 'C:\\Users\\user\\.docker\\config.json'` error in Windows, then `rm ~/.docker/config.json` and try again

### Run in Google Container Engine Kubernetes

1. Prerequisites:
    - Container Engine activated
    - Docker images pushed to Container Registry

2. Configure local Google Cloud client and `kubectl`
    - launch Google Cloud SDK Shell
    - In Windows, you need to configure `kubectl` configuration file

            set KUBECONFIG=C:\Users\marts\.kube\config

    - configure project (substitute `172711` with real ID)

            gcloud config set project parkingspot

    - set zone ([information about zones](https://cloud.google.com/compute/docs/regions-zones/regions-zones))

            gcloud config set compute/zone europe-west2-a

3. Create a cluster

    - authenticate (one time, already done):

            gcloud auth application-default login

    - open [Kubernetes cluster list](https://console.cloud.google.com/kubernetes/list) in Google Cloud Console, otherwise you might get an error
    - create the cluster

            gcloud container clusters create parkingspot-demo --machine-type=n1-standard-2 --scopes cloud-platform

4. Deploy applications to cluster

        cd kubernetes
        for app in registry console apigateway customerapi parkingapi
        do
            kubectl apply -f $app
        done

5. Watch deploy progress, logs (substitute `parkingapi-94982486-cm4fx` with real ID)

        kubectl get pods --watch
        kubectl logs parkingapi-94982486-cm4fx

6. You need to forward JHipster Registry port to local machine to access it

        kubectl port-forward jhipster-registry-0 8761:8761

7. Open [JHipster Registry](http://localhost:8761) in browser

8. Get exposed gateway application IP

        kubectl get svc apigateway

9. Open the application in browser using the output IP

10. Finally, when you are done, delete the cluster to free up resources

        for app in [acpr]*
        do
            kubectl delete -f $app
        done
        gcloud container clusters delete parkingspot-demo
