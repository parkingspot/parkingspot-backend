# JHipster generated Docker-Compose configuration

## Usage

Launch all your infrastructure by running: `docker-compose up -d`.

## Configured docker services

### Service registry and configuration server:
- [JHipster Registry](http://localhost:8761)

### Applications and dependencies:
- apigateway (gateway application)
- apigateway's postgresql database
- customerapi (microservice application)
- customerapi's postgresql database
- parkingapi (microservice application)
- parkingapi's postgresql database

### Additional Services:

- [JHipster Console](http://localhost:5601)
