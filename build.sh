#!/bin/bash

set -u
set -e

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $SCRIPT_DIR

for app in customer-api parking-api api-gateway
do
    echo "Building '$app'"
    pushd $app
    ./mvnw -DskipTests=true clean package -Pprod dockerfile:build
    popd
done

